﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using TransactionProcessor.Domain.Interface;
using static TransactionProcessor.Startup;

namespace TransactionProcessor.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionController : ControllerBase
    {
        private readonly ITransaction _csvTransactionService;
        private readonly ITransaction _xmlTtransactionService;

        public TransactionController(ServiceResolver transactionService)
        {
            _csvTransactionService = transactionService("CSV");
            _xmlTtransactionService = transactionService("XML");
        }

        [HttpPost("uploadfile")]
        public IActionResult Import(IFormFile file)
        {
            try
            {
                if (file.Length > 0)
                {
                    if (file.ContentType == "text/xml")
                    {
                        var transactionList = _xmlTtransactionService.Import(file);
                        _xmlTtransactionService.IsBalanced(transactionList);
                        var validateError = _xmlTtransactionService.Validate(transactionList);
                    }
                    else if (file.ContentType == "application/vnd.ms-excel")
                    {
                        var transactionList = _csvTransactionService.Import(file);
                        _csvTransactionService.IsBalanced(transactionList);
                        var validateError = _csvTransactionService.Validate(transactionList);
                    }
                    else
                    {
                        return Ok(new JsonResult("Please upload either XML or CSV file."));
                    }
                }

                return Ok(new JsonResult("Success Message"));
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}