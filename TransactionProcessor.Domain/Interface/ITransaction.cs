﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using TransactionProcessor.Domain.Model;

namespace TransactionProcessor.Domain.Interface
{
    public interface ITransaction
    {
        List<Transaction> Import(IFormFile file);

        List<Violation> Validate(List<Transaction> transactions);

        bool IsBalanced(List<Transaction> transactions);
    }
}