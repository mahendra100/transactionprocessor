﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TransactionProcessor.Domain.Model
{
    public class Transaction
    {
        public string Type { get; set; }
        public string Amount { get; set; }
        public string Narration { get; set; }
    }
}