﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TransactionProcessor.Domain.Model
{
    public class Violation
    {
        public int LineNumber { get; set; }
        public string FieldName { get; set; }
        public string Error { get; set; }
    }
}