﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace TransactionProcessor.Domain.Model
{
    public class TransactionMap : ClassMap<Transaction>
    {
        public TransactionMap()
        {
            Map(m => m.Type).Validate(x => x.ToLower() == "c" || x.ToLower() == "d");
            Map(m => m.Amount).Validate(x => Decimal.Parse(x) > 0);
            Map(m => m.Narration);
        }
    }
}