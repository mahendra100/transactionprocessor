﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransactionProcessor.Domain.Model;
using TransactionProcessor.Service.Implementation;

namespace TransactionProcessor.Test
{
    public class XMLValidateTest
    {
        private XMLTransaction _xMLTransaction = null;

        [SetUp]
        public void SetValues()
        {
            // ARRANGE
        }

        private List<Transaction> getTransactions()
        {
            List<Transaction> transactions = new List<Transaction>();
            transactions.Add(new Transaction { Type = "D", Amount = "10", Narration = "Manchester" });
            transactions.Add(new Transaction { Type = "C", Amount = "1300", Narration = "St. Elizabeth" });
            transactions.Add(new Transaction { Type = "D", Amount = "200", Narration = "St. Lucy" });
            return transactions;
        }

        [Test]
        [Description("Should pass validation to import XML data")]
        public async Task Validate_XML_Data_For_Amount_Field_GreaterThan_Zero()
        {
            //Setup
            var transactions = getTransactions();

            //Call
            _xMLTransaction = new XMLTransaction();

            // ACT
            var validateFile = _xMLTransaction.Validate(transactions);

            //Assert
            Assert.That(transactions.Select(c => decimal.Parse(c.Amount)), Is.All.GreaterThan(0));
        }

        [Test]
        [Description("Should pass validation to import XML data")]
        public async Task Validate_XML_Data_For_Amount_Field_Zero()
        {
            //Setup

            var transactions = getTransactions();
            transactions.Add(new Transaction { Type = "D", Amount = "0", Narration = "Manchester" });

            //Call
            _xMLTransaction = new XMLTransaction();

            // ACT
            var validateFile = _xMLTransaction.Validate(transactions);

            //Assert
            Assert.That(Decimal.Parse(transactions[3].Amount), Is.EqualTo(0));
        }

        [Test]
        [Description("Should pass validation to import XML data")]
        public async Task Validate_XML_Data_For_Type_Field()
        {
            //Setup

            var transactions = getTransactions();
            transactions.Add(new Transaction { Type = "E", Amount = "0", Narration = "Manchester" });

            //Call
            _xMLTransaction = new XMLTransaction();

            // ACT
            var validateFile = _xMLTransaction.Validate(transactions);

            //Assert
            List<string> transactionType = new List<string>() { "c", "d" };
            Assert.AreNotEqual(transactions[3].Type, transactionType.Contains(transactions[3].Type));
        }
    }
}