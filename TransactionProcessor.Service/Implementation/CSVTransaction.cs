﻿using CsvHelper;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using TransactionProcessor.Domain.Interface;
using TransactionProcessor.Domain.Model;

namespace TransactionProcessor.Service.Implementation
{
    public class CSVTransaction : ITransaction
    {
        /// <summary>
        /// Read CSV file and Deserializate to Model List.
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public List<Transaction> Import(IFormFile file)
        {
            List<Transaction> transactionList = new List<Transaction>();
            using (MemoryStream ms = new MemoryStream())
            {
                file.CopyTo(ms);
                byte[] fileBytes = ms.ToArray();
                var csv = Encoding.UTF8.GetString(fileBytes);
                var textReader = new StringReader(csv);
                var helper = new CsvReader(textReader);
                helper.Configuration.HasHeaderRecord = false;
                transactionList = helper.GetRecords<Transaction>().ToList();
            }
            return transactionList;
        }

        /// <summary>
        /// Check the Transaction is valid or not.
        /// </summary>
        /// <param name="transactions"></param>
        /// <returns></returns>
        public bool IsBalanced(List<Transaction> transactions)
        {
            decimal totalCreditTransaction = transactions.Where(x => x.Type.ToLower() == "c").Sum(t => Decimal.Parse(t.Amount));
            decimal totalDebitTransaction = transactions.Where(x => x.Type.ToLower() == "d").Sum(t => Decimal.Parse(t.Amount));
            return totalCreditTransaction == totalDebitTransaction;
        }

        /// <summary>
        /// Check Validation and Return Validation Message.
        /// </summary>
        /// <param name="transactions"></param>
        /// <returns></returns>
        public List<Violation> Validate(List<Transaction> transactions)
        {
            List<Violation> errorAmountList = getAmountValidationError(transactions);
            List<Violation> errorTypeList = getTypeValidationError(transactions);
            return errorAmountList.Union(errorTypeList).ToList();
        }

        private static List<Violation> getAmountValidationError(List<Transaction> transactions)
        {
            var errorTransaction = transactions.Where(x => decimal.Parse(x.Amount) <= decimal.Zero);
            var errorList = new List<Violation>();
            foreach (var itm in errorTransaction)
            {
                int err = transactions.IndexOf(itm);
                errorList.Add(new Violation { LineNumber = err, FieldName = "Amount", Error = "Must be greater than zero" });
            }

            return errorList;
        }

        private static List<Violation> getTypeValidationError(List<Transaction> transactions)
        {
            List<string> transactionType = new List<string>() { "c", "d" };
            var errorTransaction = transactions.Where(x => !transactionType.Contains(x.Type.ToLower()));
            var errorList = new List<Violation>();
            foreach (var itm in errorTransaction)
            {
                int err = transactions.IndexOf(itm);
                errorList.Add(new Violation { LineNumber = err, FieldName = "Type", Error = "Must be C (Credit) or D (Debit)" });
            }

            return errorList;
        }
    }
}